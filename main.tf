variable "instance_type" {
  type = string
  default = "t2.micro"
}


resource "aws_security_group" "arturo-sg" {
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Apache http port"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  name = "arturo-sg11022020"
}

resource "aws_key_pair" "arturorojas_oregon_key" {
  key_name   = "arturo.rojas11022020"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCpSOQfZe3hiPlKBNZvuf2MOMacaTuV3HagrKF1Nztbt3nAlsZeDIE/H/gIpH/yLqLTkG7A7fZpEJX9JzFxR1E6sDPYO56fq+tkmXNBMuy3vXG4TYE09T0mj4VSEGF0/LtYBF719xxikTKHLO1DnebEWAtX3lqC37DfXqT5h3F2a2BEnxfO4LcB/LjwAa1HYu4FjK1XPqou/KcdLmpms7ibmhuiWL8/NEy9YbfYlMg8p2gs5txjtLDzQfYvEfLDz0ODQR6fKH+C8JA9aAHwJzA7GpxuY3h10xVhMoTN3t1aRcoAIJZWxyDKbBEeqh+UjxQ05QQVpzewnBpgQJtSGn8n arturorojas@MacBook-Pro-de-Admin.local"
}

data "aws_ami" "amazon_ami" {
  most_recent = true
  filter {
    name = "name"
    values = ["amzn2-ami-ecs-hvm-2.0.20190603-x86_64-ebs"]
  }
  owners = ["591542846629"]
}

resource "aws_instance" "web" {
    ami = data.aws_ami.amazon_ami.id
    instance_type = var.instance_type

    vpc_security_group_ids = [aws_security_group.arturo-sg.id]
    tags = {
        Name = "Arturo-Instance11022020"
    }

    key_name = "arturo.rojas11022020"

    user_data = file("./user_data.txt")
}
output "security_group_id" {
  value = aws_security_group.arturo-sg.id
}


